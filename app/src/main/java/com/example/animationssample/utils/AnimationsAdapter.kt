package com.example.animationssample.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.animationssample.R

/** Адаптер для отображения списка анимаций на стартовом экране приложения */
class AnimationsAdapter(private val onAnimationClick: (Class<*>) -> Unit) :
    RecyclerView.Adapter<AnimationsAdapter.AnimationViewHolder>() {

    private val animations = AnimationType.values()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimationViewHolder =
        AnimationViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_animation, parent, false)
        )

    override fun getItemCount(): Int = AnimationType.values().size

    override fun onBindViewHolder(holder: AnimationViewHolder, position: Int) {
        holder.onBind(
            animationType = animations[position],
            onAnimationClick = onAnimationClick
        )
    }

    class AnimationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind(animationType: AnimationType, onAnimationClick: (Class<*>) -> Unit) {
            (itemView as AppCompatTextView).apply {
                text = itemView.context.getString(animationType.title)
            }

            itemView.setOnClickListener { onAnimationClick.invoke(animationType.activityClass) }
        }

    }

}