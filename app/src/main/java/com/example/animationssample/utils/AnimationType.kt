package com.example.animationssample.utils

import androidx.annotation.StringRes
import com.example.animationssample.R
import com.example.animationssample.activities.DrawableAnimationActivity
import com.example.animationssample.activities.LottieActivity
import com.example.animationssample.activities.MotionActivity
import com.example.animationssample.activities.ObjectAnimationActivity
import com.example.animationssample.activities.ValueAnimatorActivity

/** Предустановленный  набор констант, соответствующий  видам анимаций, используемых в проекте */
enum class AnimationType(@StringRes val title: Int, val activityClass: Class<*>) {
    MOTION_ANIMATION(R.string.motion_layout_animation, MotionActivity::class.java),
    DRAWABLE_ANIMATION(R.string.drawable_animation, DrawableAnimationActivity::class.java),
    VALUE_ANIMATOR(R.string.value_animator, ValueAnimatorActivity::class.java),
    OBJECT_ANIMATOR(R.string.object_animator, ObjectAnimationActivity::class.java),
    LOTTIE_ANIMATION(R.string.lottie_animation, LottieActivity::class.java),
}