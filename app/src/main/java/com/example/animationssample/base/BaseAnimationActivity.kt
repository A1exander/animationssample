package com.example.animationssample.base

import android.os.Bundle
import android.transition.Explode
import android.transition.Fade
import android.transition.Slide
import androidx.appcompat.app.AppCompatActivity

/** Базовая Activity для всех, включающих в себя анимации
 * Используется для создания анимации при входе на Activity */
open class BaseAnimationActivity : AppCompatActivity() {

    companion object {
        const val SHORT_ANIMATION = 1000L
        const val LONG_ANIMATION = 2000L
    }

    /** Набор из стандартных анимаций для старта Activity */
    private val animations = mutableListOf(Explode(), Fade(), Slide())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setActivityAnimations()
    }

    /** Метод устанавливает для Activity анимации входа и выхода, рандомно берущиеся из вышеопределенного списка */
    private fun setActivityAnimations() {
        with(window) {
            enterTransition = animations.random()
            exitTransition = animations.random()
        }
    }

}