package com.example.animationssample.activities

import android.os.Bundle
import com.example.animationssample.R
import com.example.animationssample.base.BaseAnimationActivity

/** Activity использующая LottieAnimation
 * Для демонстрации данного вида анимации, необходимо подключить библиотеку Lottie(добавить зависимость
 * в build.gradle уровня модуля)
 * После чего, добавить в разметку LottieAnimationView
 *
 * Данная View имеет некоторые полезные атрибуты(указаны в xml-разметке)
 * lottie_autoPlay - флаг, который указывает на то, что необходимо начать проигрывать анимацию сразу,
 * как только она загружена
 * lottie_loop - флаг, указывающий на то, будет ли анимация повторяться бесконечно
 * lottie_rawRes - атрибут, позволяющий задать непосредственно саму анимацию(Lottie позволяет
 * указать необходимую анимацию в некоторых видах - .jsonm .lottie, .zip)
 * lottie_url - позволяет указать url на требуемую анимацию
 * */
class LottieActivity : BaseAnimationActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lottie)
    }

}