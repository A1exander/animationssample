package com.example.animationssample.activities

import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.Button
import com.example.animationssample.R
import com.example.animationssample.base.BaseAnimationActivity

/** Activity использующая в качестве примера ValueAnimator
 *
 * ValueAnimator позволяет вычислять значения на основании некоторых параметров:
 *
 * - значения в пределах которых необходимо изменять что-либо, ValueAnimator имеет статические
 * методы .ofInt, .ofFloat... принимающих в качестве параметра - vararg значений(где, первое значение
 * является стартовым, а последнее - финальным, достигнув которого, анимация закончится; допускаются,
 * также, промежуточные значения, указанные, между первым и последним, они не обязательно должны быть
 * в пределах начального и конечного значения. Пример: ValueAnimator.ofInt(0, -56, 1))
 *
 * - у созданного объекта ValueAnimator, есть поле duration - отвечающее за время выполнения анимации
 *
 * - созданный объект ValueAnimator, так же, может конфигурировать Interpolator - объект, позволяющий
 * вычислить промежуточные значения анимации, с помощью него, можно контролировать ускорение/замедление анимации
 * в соответствующих местах
 * Пример(украденный с developer.android): мы хотим анимировать какую-либо View - сдвинуть на 40 пикселей вправо,
 * с длительностью анимации 40мс. При использовании линейного интерполятора(LinearInterpolator,
 * он же и используется, если мы явно не укажем никакой другой интерполятор), наша анимация будет происходить
 * с промежутком в 10мс, со смещением View на 10 пикселей. Т.е. View будет передвигаться равномерно,
 * на протяжении всей длительности анимации.
 * В той же анимации View(40мс, 40 пикселей), но при использовании другого интерполятора(например AccelerateInterpolator),
 * движение View, будет уже не линейным - начальные перемещения будут достаточно медленными, тогда как,
 * ближе к концу анимации View будет перемещаться с заметным ускорение(которе будет возрастать)
 *
 * Подробнее про виды интерполяторов, можно прочитать на developer.android
 *
 * !Важно понимать, что анимация не анимирует конретно View, анимация(в данном случае с применением ValueAnimator),
 * позволяет изменять значения в указанных промежутках с течением времени! Отсюда следует последний !обязательный!
 * пункт
 *
 * - добавление лисенера, отлавливающего вычисленные ValueAnimator'ом значения и на основании этих значений,
 * соответствующе изменять нашу View(размер, цвет, положение на оси X/Y)
 * */
class ValueAnimatorActivity : BaseAnimationActivity() {

    private lateinit var button: Button
    private lateinit var animatorSet: AnimatorSet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_value_animator)

        initView()
    }

    override fun onPause() {
        super.onPause()
        if (::animatorSet.isInitialized) animatorSet.pause()
    }

    override fun onResume() {
        super.onResume()
        if (::animatorSet.isInitialized) animatorSet.resume()
    }

    private fun initView() {
        button = findViewById<Button?>(R.id.button).apply {
            // запуск анимации происходит по нажатию на кнопку,
            // т.к. нельзя начать проигрывать анимацию до того, как все View будут расположены и отрисованы на экране
            setOnClickListener { animateButton() }
        }
    }

    // AnimatorSet - инструмент, позволяющий контролировать запуск нескольких анимаций сразу
    // с его помощью, можно запустить анимации одновременно, либо одну за/перед другой
    private fun animateButton() {
        if (::animatorSet.isInitialized.not()) {
            animatorSet = AnimatorSet().apply {
                play(animateAlpha())
                play(animateWidth())
                play(animateHeight())
            }
        }

        // важный момент, что анимации описываемые с помощью ValueAnimator нужно явно запустить
        // вызвав метод start
        animatorSet.start()
    }

    // метод, анимирующмй высоту кнопки
    // в качестве значений, между которыми будут происходить изменения, выбраны - текущая высота View(стартовое значение),
    // двойная высота View, среднее значение между высотой View и двойной высотой View(конечное значени)
    // В результате анимации, у View в 2 раза увеличится высота, а затем уменьшится на 25%
    private fun animateHeight(): ValueAnimator = ValueAnimator.ofInt(
        button.height,
        button.height * 2,
        button.height + button.height / 2
    ).apply {
        // устанавливаем время выполнения анимации в 1 секунду
        duration = SHORT_ANIMATION

        // указываем линейный интерполятор(в данном случае, его даже можно опустить и не указывать совсем)
        // анимация будет выполняться с одинаковой скоростью на протяжении всего времени выполнения
        interpolator = LinearInterpolator()

        // добавляем слушатель изменных значений
        addUpdateListener {
            // конвертируем значение в нужный нам тип
            val value = it.animatedValue as Int

            // применяем вычесленное ValueAnimator'ом значение к нашей View
            button.layoutParams = button.layoutParams.apply {
                height = value
            }
        }
    }

    // метод, анимирующмй ширину кнопки
    // в качестве значений, между которыми будут происходить изменения, выбраны - текущая ширина View(стартовое значение),
    // тройная ширина View, половина тройной ширины View(конечное значени)
    // В результате анимации, у View в 3 раза увеличится ширина, а затем уменьшится в 2 раза
    private fun animateWidth(): ValueAnimator = ValueAnimator.ofInt(
        button.width,
        button.width * 3,
        button.width + button.width / 2
    ).apply {
        // устанавливаем время выполнения анимации в 1 секунду
        duration = SHORT_ANIMATION

        // указываем Accelerate интерполятор
        // анимация будет выполняться с ускорением
        interpolator = AccelerateInterpolator()

        // добавляем слушатель изменных значений
        addUpdateListener {
            // конвертируем значение в нужный нам тип
            val value = it.animatedValue as Int

            // применяем вычесленное ValueAnimator'ом значение к нашей View
            button.layoutParams = button.layoutParams.apply {
                width = value
            }
        }
    }

    // метод, анимирующмй прозрачность кнопки
    // В результате анимации, у View из полностью прозрачной, станет полностью видимой, а затем, станет полупрозрачной
    private fun animateAlpha(): ValueAnimator = ValueAnimator.ofFloat(0f, 1f, 0.5f).apply {
        // устанавливаем время выполнения анимации в 2 секунды
        duration = LONG_ANIMATION

        // указываем AccelerateDecelerate интерполятор
        // анимация сначала будет ускоряться, а приближаясь к концу - замедляться
        interpolator = AccelerateDecelerateInterpolator()

        // добавляем слушатель изменных значений
        addUpdateListener {
            // конвертируем значение в нужный нам тип и применяем вычесленное ValueAnimator'ом значение к нашей View
            button.alpha = it.animatedValue as Float
        }
    }

}