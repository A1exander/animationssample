package com.example.animationssample.activities

import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.widget.ImageView
import com.example.animationssample.R
import com.example.animationssample.base.BaseAnimationActivity

/** Activity показывающая пример использования DrawableAnimation
 *
 * Для осуществления такой анимации используется набор drawable-файлов, каждый из которых описывает
 * один кадр из анимации
 *
 * Все drawable-файлы(кадры) совмещаются в файле, рут-элементом которого является animation-list
 *
 * В данном примере, таким файлом с набором кадров явлется - arrow_list.xml
 */
class DrawableAnimationActivity : BaseAnimationActivity() {

    private lateinit var arrowAnimation: AnimationDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawable_animation)

        // находим нашу View, фоном для которой будет drawable-animation
        val imageView = findViewById<ImageView>(R.id.iv_drawable).apply {
            // устанавливаем фоном файл с набором кадров анимации
            // так же, этот можно сделать и в xml-разметке(тег background )
            setBackgroundResource(R.drawable.arrow_list)
            // получаем ссылку на наш AnimationDrawable, для того, чтобы мы могли запустить анимацию
            arrowAnimation = background as AnimationDrawable
        }

        // запускаем или останавливаем анимацию по клику
        imageView.setOnClickListener {
            if (arrowAnimation.isRunning) {
                arrowAnimation.start()
            } else {
                arrowAnimation.stop()
            }
        }
    }

}