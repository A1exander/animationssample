package com.example.animationssample.activities

import android.os.Bundle
import com.example.animationssample.R
import com.example.animationssample.base.BaseAnimationActivity

/** Activity используящая в качестве примера MotionLayout
 *
 * MotionLayout использует 2 constraint-set'a, которые описываю начальное и конечное положение
 * View-элементов на экране
 *
 * Более подробное описание находится в файле activity_main_scene.xml
 */
class MotionActivity : BaseAnimationActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_motion)
    }

}