package com.example.animationssample.activities

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.animationssample.R
import com.example.animationssample.base.BaseAnimationActivity
import com.example.animationssample.utils.AnimationsAdapter

/** Базовая Activity - главный экран приложения, показывающий список доступных анимаций */
class MainActivity : BaseAnimationActivity() {

    // создаем адаптер, для отображения списка анимаций и обрабатываем клики по соответствующим элементам
    private val animationAdapter = AnimationsAdapter {
        val intent = Intent(this, it)

        startActivity(
            intent,
            ActivityOptions.makeSceneTransitionAnimation(this@MainActivity).toBundle()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
    }

    private fun initViews() {
        findViewById<RecyclerView>(R.id.rv_animations).apply {
            adapter = animationAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

}