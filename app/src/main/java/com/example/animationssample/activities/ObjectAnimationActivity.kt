package com.example.animationssample.activities

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Path
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import com.example.animationssample.R
import com.example.animationssample.base.BaseAnimationActivity

/** Acitivity использующая в качестве примера ObjectAnimator
 *
 * Принцип работы аналогичен ValueAnimator, описанный в
 * @see com.example.animationssample.activities.ValueAnimatorActivity
 * За исключением того, что в качестве параметра, указывается объект, свойство которого мы
 * хотим анимировать, а так же, название этого свойства в формате строки
 *
 * Важное уточнение, данное свойство должно иметь публичные get и set методы,
 * которые будут соответствовать схеме getPROPERTY_NAME и setPROPERTY_NAME
 *
 * Также, для такой анимации, нам больше не требуется слушатель вычисленных значений
 */
class ObjectAnimationActivity : BaseAnimationActivity() {

    private lateinit var button: AppCompatButton
    private lateinit var animatorSet: AnimatorSet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_object_animator)

        initView()
    }

    override fun onPause() {
        super.onPause()
        if (::animatorSet.isInitialized) animatorSet.pause()
    }

    override fun onResume() {
        super.onResume()
        if (::animatorSet.isInitialized) animatorSet.resume()
    }

    private fun initView() {
        button = findViewById<AppCompatButton>(R.id.button).apply {
            setOnClickListener { animateButton() }
        }
    }

    // AnimatorSet - инструмент, позволяющий контролировать запуск нескольких анимаций сразу
    // с его помощью, можно запустить анимации одновременно, либо одну за/перед другой
    private fun animateButton() {
        if (::animatorSet.isInitialized.not()) {
            animatorSet = AnimatorSet().apply {
                play(animatePosition())
                play(animateAlpha())
            }
        }

        // важный момент, что анимации описываемые с помощью ObjectAnimator нужно явно запустить
        // вызвав метод start
        animatorSet.start()
    }

    // метод, анимирующмй позицию кнопки(в данном случае в виде арки)
    private fun animatePosition(): ObjectAnimator {
        // описываем путь нашей кнопки, с соответствующими углами
        val path = Path().apply {
            arcTo(0f, 0f, 1000f, 1000f, 270f, -180f, true)
        }

        // создаем ObjectAnimator, в качестве объекта у которого мы будем изменять свойства, указываем
        // кнопку, а после указываем сами свойства, которые будут изменяться
        return ObjectAnimator.ofFloat(button, View.X, View.Y, path).apply {
            // устанавливаем время выполнения анимации в 2 секунды
            duration = LONG_ANIMATION
        }
    }

    // метод, анимирующмй прозрачность кнопки
    private fun animateAlpha(): ObjectAnimator {
        // создаем ObjectAnimator, в качестве объекта у которого мы будем изменять свойства, указываем
        // кнопку, а после указываем свойство прозрачности кнопки, а также, набор значений, между которыми будет
        // происходить анимация
        return ObjectAnimator.ofFloat(button, "alpha", 0f, 1f, 0f, 1f).apply {
            // устанавливаем время выполнения анимации в 2 секунды
            duration = LONG_ANIMATION
        }
    }

}